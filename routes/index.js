var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/engineering_pa', function(req, res, next) {
  res.render('engineering_pa', { title: 'Express' });
});

router.get('/smart_tiny_house', function(req, res, next) {
  res.render('smart_tiny_house', { title: 'Express' });
});

router.get('/self_driving', function(req, res, next) {
  res.render('self_driving', { title: 'Express' });
});

router.get('/wind_turbine', function(req, res, next) {
  res.render('wind_turbine', { title: 'Express' });
});

router.get('/pbp', function(req, res, next) {
  res.render('pbp', { title: 'Express' });
});


module.exports = router;