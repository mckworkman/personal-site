//CHANGE THIS SO IT DOESN"T REPT IN EVERY SCRIPT :-/
// Create layout variables
var birth_date = new Date();
birth_date.setFullYear(1992, 0, 12);
var date_current = new Date(new Date().setYear(new Date().getFullYear() + 1));

var date_top = date_current;
//If i is odd, add 2 years to it
if (date_top.getFullYear()%2 != 0) {
    date_top.setFullYear(date_top.getFullYear() + 1);
}

var width = document.getElementById('timeline').offsetWidth;
var height = document.getElementById('timeline').offsetHeight;
var y_buffer = height/70;
var line_length = height - (y_buffer*3)

// Create colors for theme
var invisible = new Color(255,255,255, 0);
var transparent_white = new Color(255,255,255, .8);
var transparent_blue = new Color(0,0,128, .6);
var blue = new Color(0,0,128);
var white = new Color(255,255,255);
var selected_color = new Color(255,255,255);

//Create Font characteristics for theme
var font = 'Arial';
var weight = 'normal';

var q3_tc = new Point(view.center.x, 0+y_buffer);

// Event Data
var proj = {
        "4h" : 
        {
            "title":"JK Arabian Stables 4H Leader",
            "subtitle": "Lead a 4-H program at JK Arabian Stables",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2000'),
            "date_finish": new Date('Jan 1, 2002'),
            "image": "../images/canvas_pics/4h.jpg"
        },
        "parelli" : 
        {
            "title":"Parelli Natural Horsemanship",
            "subtitle": "Practiced and taught natural horse-man-ship",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2002'),
            "date_finish": new Date('Jan 1, 2008'),
            "image": "../images/canvas_pics/tedbeach.jpg"
        },
        "sci_girls" : 
        {
            "title":"Sci Girls Summer Camp",
            "subtitle": "Summer Camp with the FSU Magnet Lab",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2003'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/maglab.jpg"
        },
        "prop_scars" : 
        {
            "title": "Propellar Scars off Turkey Point",
            "subtitle": "Studied a timelapse of propscars off Turkey Point, FL",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2004'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": null
        },
        "nasas_great_moonbuggy_race" : 
        {
            "title":"NASA's Great Moonbuggy Race!",
            "subtitle": "Designed, built and co-piloted with the NFCC moonbuggy team :)",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2005'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/moonbuggy.jpg"
        },
        "green_industries" : 
        {
            "title":"Green Industries Internship",
            "subtitle": "Researched EMERGY analysis for the site",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2006'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/gi.jpg"
        },
        "new_moon_nights" : 
        {
            "title":"Hosted 'New Moon Nights'",
            "subtitle": "Educational workshop once/month on the new moon at GI observatory",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2007'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/nmn.png"
        },
        "italian ice" : 
        {
            "title":"Peace, Love & Italian Ice, LLC",
            "subtitle": "Started an Italian Ice Vending Business",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2008'),
            "date_finish": new Date('Jan 1, 2010'),
            "image": "../images/canvas_pics/pli.png"
        },
        "real estate business" : 
        {
            "title":"Destiny Investment Group, LLC",
            "subtitle": "Became a partner in a realestate business flipping houses",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2009'),
            "date_finish": new Date('Jan 1, 2010'),
            "image": null
        },
        "bonsai_banyans" : 
        {
            "title":"Parametric Design Project",
            "subtitle": "Parametric design project with Grasshopper3D and Arduino",
            "location": "Boca Raton, FL",
            "date_start": new Date('Feb 1, 2010'),
            "date_finish": new Date('March 1, 2010'),
            "image": "../images/canvas_pics/grasshopper.png"
        },
        "robotic chess" : 
        {
            "title":"Robotic Chess Game",
            "subtitle": "Joinged programming team to code for a robotic chess game at FAU",
            "location": "Boca Raton, FL",
            "date_start": new Date('Dec 1, 2009'),
            "date_finish": new Date('March 1, 2010'),
            "image": "../images/canvas_pics/robotic_chess.png"
        },
        "wind turbine" : 
        {
            "title":"Beach Front Wind Turbine",
            "subtitle": "Built a wind turbine with treadmill motor",
            "location": "Sarasota, FL",
            "date_start": new Date('Jan 1, 2011'),
            "date_finish": new Date('March 1, 2011'),
            "image": null
        },
        "rc" : 
        {
            "title":"DIY Foam RC Craft",
            "subtitle": "Built a foam UAV and tried to fly it...crashed",
            "location": "Sarasota, FL",
            "date_start": new Date('March 1, 2011'),
            "date_finish": new Date('March 1, 2011'),
            "image": "../images/canvas_pics/rc.png"
        },
        "grid_alternatives" : 
        {
            "title":"Grid Alternatives Installation",
            "subtitle": "Assisted installing solar systems for houses!",
            "location": "San Luis Obispo, FL",
            "date_start": new Date('Jan 1, 2012'),
            "date_finish": new Date('March 1, 2012'),
            "image": "../images/canvas_pics/grid_alternatives.jpg"
        },
        "white_hat_cyber_club" : 
        {
            "title":"WhiteHat Cybersecurity Club",
            "subtitle": "Fuzzers, network monitoring software, exploits, SCADA, metasploit, backtrack, pentoo, plcs",
            "location": "San Luis Obispo, CA",
            "date_start": new Date('March 1, 2012'),
            "date_finish": new Date('March 1, 2012'),
            "image": "../images/canvas_pics/white_hat.png"
        },
        "bee_keeping" : 
        {
            "title":"Bee Keeping",
            "subtitle": "Started keeping a few hundred thousand bees",
            "location": "Orlando, FL",
            "date_start": new Date('Jan 1, 2014'),
            "date_finish": new Date(),
            "image": "../images/canvas_pics/bees.jpg"
        },
        "at2" :
        {
            "title":"Appropriate Tech Toolkit",
            "subtitle": "Building an appropriate technology toolkit (see 'featured projects')",
            "location": "Orlando, FL",
            "date_start": new Date('Dec 1, 2014'),
            "date_finish": new Date(),
            "image": null
        },
        "epa" : 
        {
            "title":"Engineering Personal Assistant",
            "subtitle": "Building an EPA see (see 'FEATURED PROJECTS')",
            "location": "Orlando, FL",
            "date_start": new Date('May 1, 2015'),
            "date_finish": new Date(),
            "image": null
        },
        "smart_tiny_house" :
        {
            "title":"Smart Tiny House",
            "subtitle": "Writing software for a house with a central intellegence unit (see 'FEATURED PROJECTS')",
            "location": "Orlando, FL",
            "date_start": new Date('Aug 1, 2015'),
            "date_finish": new Date(),
            "image": null
        }
};

function add_projects_layout() {
    //Create a line and title for Projects
    var project_line = new Path( {
        strokeColor: white,
        strokeWidth: 5,
        strokeCap: 'round'
    })
    
    project_line.moveTo(q3_tc);
    project_line.lineTo(q3_tc+[0,line_length]);
}

function get_max_events() {
    // Returns the maximum number of events for edu, travel and proj objects
    var max = 0;
    var i = 0;
    for (var key in proj) { i++; }
    if (i > max) { max = i; }
    i=0;
    for (var key in edu) { i++; }
    if (i > max) { max = i; }
    i=0;
    for (var key in travel) { i++; }
    if (i > max) { max = i; }
    return max;
}

function get_segments(str, max_chars) {
    //Split strings into spaces
    var split_str = str.split(" ");
    var segments = [""];
    
    // While the end of the array isn't reached
    var i = 0;
    var j = 0;
    while (split_str[i]) {
        // If there is room to add another word, add it
        if ( (segments[j]+ " " + split_str[i]).length < max_chars) {
            //Add another word to segments j
            segments[j] = segments[j] + " " + split_str[i];
            i++;
        } else {
            // Increment segments and i
            segments.push(split_str[i]);
            j++;
            i++;
        }
    }
    // console.log("Final Segments: "+segments);
    return segments
}

// Get total number of months in years from birth
var months = (date_top.getFullYear() - birth_date.getFullYear()) * 12;
    months -= birth_date.getMonth();
    months += date_top.getMonth();
    months <= 0 ? 0 : months;

function add_simple_event(quadrant, obj) {
    var num_months = (obj.date_start.getFullYear() - birth_date.getFullYear()) * 12;
    num_months -= birth_date.getMonth();
    num_months += obj.date_start.getMonth();
    num_months <= 0 ? 0 : num_months;
    
    var new_pt = new Point( q3_tc.x, line_length - ( (line_length/months)*num_months ) );
    
    //Add circle
    var edu_event = new Shape.Circle(new_pt, 8);
    edu_event.strokeColor = white;
    edu_event.strokeWidth = 5;
    
    //Add text
    var title = new PointText({
        point: new_pt+[20,0],
        content: obj.title,
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 12
    });
    
    //Create the background rect for the image MAYBE GET RID OF THIS!!
    var big_rect_w = 150;
    var big_rect_h = -120;
    var size = new Size(big_rect_w, big_rect_h);
    var background_rect = new Path.Rectangle((new_pt+[-175,50]), size);
    background_rect.fillColor = white;
    background_rect.visible = false;
    
    //Create the object image and assign it a source
    var raster = new Raster();
    if (obj.image == null) { raster.source = '../images/canvas_pics/no-image.png'; } 
    else { raster.source = obj.image; }
    raster.visible = false;
    raster.bringToFront();
    raster.position = background_rect.bounds.center;
    
    //Add the subtitle information below the image
    var group = new Group();
    var i = 0;
    var text_rect = new Rectangle();
    if (obj.subtitle != null) {
        //Chars_per_line should be changed to be a function of box width
        var chars_per_line = 25;
        var segments = get_segments(obj.subtitle, chars_per_line);
        for (i=0; i<segments.length;i++) {
            // Make a line of text for each string segment
            var text = new PointText({
                content: segments[i],
                position: (background_rect.bounds.bottomCenter + [0, title.bounds.height*(i+1)]),
                fillColor: invisible,
                // visible: false,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 11,
            });
            //Add this as a child to subtitle
            group.addChild(text);
        }
        
        // Create the background rect for the text
        var size = new Size(big_rect_w,title.bounds.height*(i+1));
        var text_rect = new Path.Rectangle(background_rect.bounds.bottomLeft, size, new Size(5,5));
        text_rect.fillColor = white;
        text_rect.visible = false;
        text_rect.sendToBack();
    }
    
    title.on('mousemove', function() {
        //Bold the text that is selected
        this.fontWeight = 'bold',
        edu_event.strokeColor = transparent_blue,
        raster.visible = true,
        group.fillColor = blue,
        // background_rect.visible = true,
        text_rect.visible = true
    });
    
    title.on('mouseleave', function() { 
        this.fontWeight = weight,
        edu_event.strokeColor = white,
        raster.visible = false,
        group.fillColor = invisible,
        // background_rect.visible = false,
        text_rect.visible = false
    });
}

function add_project_events() {
    for (var key in proj) {
        if (proj.hasOwnProperty(key)) {
            var obj = proj[key];
            add_simple_event(3, obj);
        }
    }
}

function onMouseMove(event) {
	project.activeLayer.selected = false;
	if (event.item) {
	    // For debugging only
		//event.item.selected = true;
	}
}


add_projects_layout();
add_project_events();