var edu = {
        home_school : 
        {
            "title":"Home School",
            "subtitle": null,
            "location": "Tallahassee, FL",
            "year_start": 1992,
            "year_finish": 2006,
            "image": null
        },
        high_school :
        {
            "title":"Rickards High",
            "subtitle": "International Baccalaureate Program",
            "location": "Tallahassee, FL",
            "year_start": 2006,
            "year_finish": 2008,
            "image": null
        },
        nfcc :
        {
            "title":"North FL Community College",
            "subtitle": null,
            "location": "Madison, FL",
            "year_start": 2008,
            "year_finish": 2010,
            "image": null
        },
        ncf :
        {
            "title":"New College of FL",
            "subtitle": null,
            "location": "Sarasota, FL",
            "year_start": 2010,
            "year_finish": 2012,
            "image": null
        }
};