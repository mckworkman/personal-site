// Create layout variables
var birth_date = new Date();
birth_date.setFullYear(1992, 0, 12);
var date_current = new Date(new Date().setYear(new Date().getFullYear()));

var date_top = date_current;
//If i is odd, add 2 years to it
if (date_top.getFullYear()%2 != 0) {
    date_top.setFullYear(date_top.getFullYear() + 1);
}

var width = document.getElementById('timeline').offsetWidth;
var height = document.getElementById('timeline').offsetHeight;
var y_buffer = height/70;
var line_length = height - (y_buffer*3)

// Create colors for theme
var invisible = new Color(255,255,255, 0);
var transparent_white = new Color(255,255,255, .8);
var transparent_blue = new Color(0,0,128, .6);
var blue = new Color(0,0,128);
var white = new Color(255,255,255);
var selected_color = new Color(255,255,255);

//Create Font characteristics for theme
var font = 'Arial';
var weight = 'normal';

var q1_c = new Point(view.center.x, view.center.y);
var q1_tc = new Point(view.center.x, 0+y_buffer);

// Get total number of months in years from birth
var months = (date_top.getFullYear() - birth_date.getFullYear()) * 12;
    months -= birth_date.getMonth();
    months += date_top.getMonth();
    months <= 0 ? 0 : months;
    
function add_layout() {
    
    for (i = 0; i<months; i=i+12) {
        var year = birth_date.getFullYear() + i/12;
        if (year%2 != 0) {
            var date_text = new PointText({
                point: q1_tc + [0, line_length - ( (line_length/months)*i ) ],
                // content: "____",
                content: year,
                fillColor: white,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 25
            });
        } else {
            var date_text = new PointText({
                point: q1_tc + [0, line_length - ( (line_length/months)*i) ],
                content: year,
                fillColor: white,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 25
                });
        }
    }
}

add_layout();