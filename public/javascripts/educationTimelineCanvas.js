//CHANGE THIS SO IT DOESN"T REPT IN EVERY SCRIPT :-/
// Create layout variables
var birth_date = new Date();
birth_date.setFullYear(1992, 0, 12);
var date_current = new Date(new Date().setYear(new Date().getFullYear()));

var date_top = date_current;
//If i is odd, add 2 years to it
if (date_top.getFullYear()%2 != 0) {
    date_top.setFullYear(date_top.getFullYear() + 1);
}

var width = document.getElementById('timeline').offsetWidth;
var height = document.getElementById('timeline').offsetHeight;
var y_buffer = height/70;
var line_length = height - (y_buffer*3)

// Create colors for theme
var invisible = new Color(255,255,255, 0);
var transparent_white = new Color(255,255,255, .8);
var transparent_blue = new Color(0,0,128, .6);
var blue = new Color(0,0,128);
var white = new Color(255,255,255);
var selected_color = new Color(255,255,255);

//Create Font characteristics for theme
var font = 'Arial';
var weight = 'normal';

var q2_tc = new Point(view.center.x-20, 0+y_buffer);

// Event Data
// Must be in chronological order
var edu = {
        "home_school" : 
        {
            "title":"Home School",
            "subtitle": "Pre-K and Kindergarden",
            "location": "Tallahassee, FL",
            "date_start": new Date('April 1, 1992'),
            "date_finish": new Date('Jan 1, 1998'),
            "image": "../images/canvas_pics/home_school.jpg"
        },
        "florida_high" : 
        {
            "title":"Florida State University Elementary",
            "subtitle": "1st-4th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 1, 1998'),
            "date_finish": new Date('Aug 1, 2002'),
            "image": "../images/canvas_pics/florida_high.jpg"
        },
        "sealey" : 
        {
            "title":"Sealey Elementary School",
            "subtitle": "5th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 1, 2002'),
            "date_finish": new Date('Aug 1, 2003'),
            "image": "../images/canvas_pics/sealey.jpg"
        },
        "raa_middle" : 
        {
            "title":"RAA Middle School",
            "subtitle": "6th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 1, 2003'),
            "date_finish": new Date('Aug 1, 2004'),
            "image": "../images/canvas_pics/raa.png"
        },
        "bellview" : 
        {
            "title":"Quantum Math and Science Program",
            "subtitle": "7th-8th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('April 1, 2004'),
            "date_finish": new Date('Jan 1, 2006'),
            "image": null
        },
        "high_school" :
        {
            "title":"Rickards IB Program",
            "subtitle": "International Baccalaureate Program",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 18, 2006'),
            "date_finish": new Date('Aug 18, 2008'),
            "image": "../images/canvas_pics/rickards.jpg"
        },
        "wia" :
        {
            "title":"Wealth Intellegence Academy",
            "subtitle": "Took WIA classes for business and realestate education",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 18, 2007'),
            "date_finish": new Date('Aug 18, 2008'),
            "image": "../images/canvas_pics/wia.png"
        },
        "nfcc" :
        {
            "title":"North FL Community College",
            "subtitle": null,
            "location": "Madison, FL",
            "date_start": new Date('Aug 18, 2008'),
            "date_finish": new Date('Aug 18, 2010'),
            "image": "../images/canvas_pics/nfcc.png"
        },
        "ncf" :
        {
            "title":"New College of FL",
            "subtitle": "Area of Concentration: Chemistry, Comp Sci",
            "location": "Sarasota, FL",
            "date_start": new Date('Aug 18, 2010'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/ncf.png"
        },
        "fab_lab_volunteer" :
        {
            "title":"Faulhabber Fab Lab Volunteer",
            "subtitle": "Volunteered at the Faulhabber Fab Lab",
            "location": "Sarasota, FL",
            "date_start": new Date('Dec 18, 2010'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/fab_lab.png"
        },
        "mlbco" :
        {
            "title":"MLB Co UAV Internship",
            "subtitle": "NASA airspace simulation internship",
            "location": "Mountain View, CA",
            "date_start": new Date('Feb 18, 2011'),
            "date_finish": new Date('June 18, 2012'),
            "image": "../images/canvas_pics/mlbco.jpg"
        },
        "nwnh" :
        {
            "title":"Northwest Natural Horsemanship",
            "subtitle": "Horse training",
            "location": "Fall City, WA",
            "date_start": new Date('June 30, 2011'),
            "date_finish": new Date('July 30, 2012'),
            "image": "../images/canvas_pics/nwnh.png"
        },
        "coursera_cryptography" :
        {
            "title":"Cryptography Coursera Course",
            "subtitle": "Cryptography I with Dan Boneh",
            "location": "Online",
            "date_start": new Date('Oct 18, 2011'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/coursera.png"
        },
        "coursera_nlp" :
        {
            "title":"Coursera Natural Lang Processing",
            "subtitle": "NLP Course with Coursera",
            "location": "Online",
            "date_start": new Date('Dec 18, 2011'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/coursera.png"
        },
         "calpoly" :
        {
            "title":"California Polytechnic University",
            "subtitle": "Major: General Engineering, Minor: CS",
            "location": "San Luis Obispo, CA",
            "date_start": new Date('Aug 18, 2012'),
            "date_finish": new Date('April 1, 2014'),
            "image": "../images/canvas_pics/calpoly.jpg"
        }, 
        "ewb_cp" : 
        {
            "title":"EWB Thailand Project",
            "subtitle": "Part of the Cal Poly EWB Thailand team",
            "location": "San Luis Obispo, CA",
            "date_start": new Date('Feb 1, 2012'),
            "date_finish": new Date('March 1, 2012'),
            "image": "../images/canvas_pics/ewb_calpoly.png"
        },
        "raytheon" :
        {
            "title":"Raytheon Cybersecurity Internship",
            "subtitle": "Cybersecurity Internship",
            "location": "Los Angeles, CA",
            "date_start": new Date('April 18, 2013'),
            "date_finish": new Date('July 30, 2013'),
            "image": "../images/canvas_pics/raytheon.png"
        }, 
        "ucf" :
        {
            "title":"University of Central FL",
            "subtitle": "Major: Mech Eng, Minors: CS, IT",
            "location": "Orlando, FL",
            "date_start": new Date('Aug 18, 2013'),
            "date_finish": new Date('April 30, 2016'),
            "image": "../images/canvas_pics/ucf.jpg"
        },
        "ruby" : 
        {
            "title":"Taught a Ruby on Rails Workshop",
            "subtitle": "Taught a RoR workshop for Engineers without Borders",
            "location": "Orlando, FL",
            "date_start": new Date('Dec 1, 2014'),
            "date_finish":new Date('Dec 1, 2014'),
            "image": "../images/canvas_pics/ruby_on_rails.jpg"
        },
        "ewb_pres" : 
        {
            "title":"Engineers without Borders UCF President",
            "subtitle": "Elected president of EWB",
            "location": "Orlando, FL",
            "date_start": new Date('Jan 1, 2014'),
            "date_finish": new Date('Jun 11, 2015'),
            "image": "../images/canvas_pics/ewb.png"
        },
        "co_bolder" : 
        {
            "title":"Design Global Engineer Local",
            "subtitle": "Course through University Co Boulder in Panama!",
            "location": "Panama City, Panama",
            "date_start": new Date('March 1, 2014'),
            "date_finish": new Date('Jun 11, 2015'),
            "image": "../images/canvas_pics/panama.jpg"
        },
        "drafting_skill_share" : 
        {
            "title":"Taught a Drafting Skill Share",
            "subtitle": "Taught a Drafting Skill Share for Engineers without Borders",
            "location": "Orlando, FL",
            "date_start": new Date('June 1, 2014'),
            "date_finish":new Date('Oct 1, 2014'),
            "image": "../images/canvas_pics/drafting_ss.png"
        },
        "solar_skill_share" : 
        {
            "title":"Taught a Solar Skill Share",
            "subtitle": "Taught a Solar Skill Share for Engineers without Borders",
            "location": "Orlando, FL",
            "date_start": new Date('Oct 1, 2014'),
            "date_finish":new Date('Oct 1, 2014'),
            "image": "../images/canvas_pics/solar_ss.jpg"
        },
        "coursera_mobile_apps" :
        {
            "title":"Mobile Apps Coursera Course",
            "subtitle": "Writting Android Apps",
            "location": "Sarasota, FL",
            "date_start": new Date('Jun 18, 2015'),
            "date_finish": new Date('Aug 18, 2015'),
            "image": "../images/canvas_pics/coursera.png"
        },
        "udacity_self_driving_cars" :
        {
            "title":"Self Driving Cars Udacity Course",
            "subtitle": "Artificial Intelligence for Robotics Udacity Course",
            "location": "Online",
            "date_start": new Date('Aug 18, 2015'),
            "date_finish": new Date('July 18, 2015'),
            "image": "../images/canvas_pics/udacity.png"
        }
};

function add_education_layout() {
    //Create a line and title for Education
    var education_line = new Path( {
        strokeColor: white,
        strokeWidth: 5,
        strokeCap: 'round'
    })
    
    education_line.moveTo(q2_tc);
    education_line.lineTo(q2_tc+[0,line_length]);
}

// function get_max_events() {
//     // Returns the maximum number of events for edu, travel and proj objects
//     var max = 0;
//     var i = 0;
//     for (var key in edu) { i++; }
//     if (i > max) { max = i; }
// }

function get_segments(str, max_chars) {
    //Split strings into spaces
    var split_str = str.split(" ");
    var segments = [""];
    
    // While the end of the array isn't reached
    var i = 0;
    var j = 0;
    while (split_str[i]) {
        // If there is room to add another word, add it
        if ( (segments[j]+ " " + split_str[i]).length < max_chars) {
            //Add another word to segments j
            segments[j] = segments[j] + " " + split_str[i];
            i++;
        } else {
            // Increment segments and i
            segments.push(split_str[i]);
            j++;
            i++;
        }
    }
    // console.log("Final Segments: "+segments);
    return segments
}

// Get total number of months in years from birth
var months = (date_top.getFullYear() - birth_date.getFullYear()) * 12;
    months -= birth_date.getMonth();
    months += date_top.getMonth();
    months <= 0 ? 0 : months;

function add_simple_event(quadrant, obj) {
    var num_months = (obj.date_start.getFullYear() - birth_date.getFullYear()) * 12;
    num_months -= birth_date.getMonth();
    num_months += obj.date_start.getMonth();
    num_months <= 0 ? 0 : num_months;
    
    var new_pt = new Point( q2_tc.x, line_length - ( (line_length/months)*num_months ) );
    
    //Add circle
    var edu_event = new Shape.Circle(new_pt, 8);
    edu_event.strokeColor = white;
    edu_event.strokeWidth = 5;
    
    //Add text
    var title = new PointText({
        point: new_pt+[20,0],
        content: obj.title,
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 12
    });
    
    //Create the background rect for the image MAYBE GET RID OF THIS!!
    var big_rect_w = 150;
    var big_rect_h = -120;
    var size = new Size(big_rect_w, big_rect_h);
    var background_rect = new Path.Rectangle((new_pt+[-175,50]), size);
    background_rect.fillColor = white;
    background_rect.visible = false;
    
    //Create the object image and assign it a source
    var raster = new Raster();
    if (obj.image == null) { raster.source = '../images/canvas_pics/no-image.png'; } 
    else { raster.source = obj.image; }
    raster.visible = false;
    raster.bringToFront();
    raster.position = background_rect.bounds.center;
    
    //Add the subtitle information below the image
    var group = new Group();
    var i = 0;
    var text_rect = new Rectangle();
    if (obj.subtitle != null) {
        //Chars_per_line should be changed to be a function of box width
        var chars_per_line = 25;
        var segments = get_segments(obj.subtitle, chars_per_line);
        for (i=0; i<segments.length;i++) {
            // Make a line of text for each string segment
            var text = new PointText({
                content: segments[i],
                position: (background_rect.bounds.bottomCenter + [0, title.bounds.height*(i+1)]),
                fillColor: invisible,
                // visible: false,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 11,
            });
            //Add this as a child to subtitle
            group.addChild(text);
        }
        
        // Create the background rect for the text
        var size = new Size(big_rect_w,title.bounds.height*(i+1));
        var text_rect = new Path.Rectangle(background_rect.bounds.bottomLeft, size, new Size(5,5));
        text_rect.fillColor = white;
        text_rect.visible = false;
        text_rect.sendToBack();
    }
    
    title.on('mousemove', function() {
        //Bold the text that is selected
        this.fontWeight = 'bold',
        edu_event.strokeColor = transparent_blue,
        raster.visible = true,
        group.fillColor = blue,
        // background_rect.visible = true,
        text_rect.visible = true
    });
    
    title.on('mouseleave', function() { 
        this.fontWeight = weight,
        edu_event.strokeColor = white,
        raster.visible = false,
        group.fillColor = invisible,
        // background_rect.visible = false,
        text_rect.visible = false
    });
}

function add_education_events() {
    for (var key in edu) {
        if (edu.hasOwnProperty(key)) {
            var obj = edu[key];
            add_simple_event(2, obj);
        }
    }
}

function onMouseMove(event) {
	project.activeLayer.selected = false;
	if (event.item) {
	    // For debugging only
		//event.item.selected = true;
	}
}


add_education_layout();
add_education_events();