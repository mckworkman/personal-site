var x_buffer = view.size.width/50;
var y_buffer = view.size.height/80;
var birth_date = new Date();
birth_date.setFullYear(1992, 0, 12);
// var date_current = new Date();
var date_current = new Date(new Date().setYear(new Date().getFullYear() + 1));
var width = view.size.width;
var height = view.size.height;
var age = date_current.getFullYear()-birth_date.getFullYear();
var line_length = height - (y_buffer*3)

//Create colors for theme
var invisible = new Color(255,255,255, 0);
var transparent_white = new Color(255,255,255, .8);
var transparent_blue = new Color(0,0,128, .6);
var blue = new Color(0,0,128);
var white = new Color(255,255,255);
var selected_color = new Color(255,255,255);

//Create Font characteristics for theme
var font = 'Arial';
var weight = 'normal';

//Rectangle around canvas for DEBUGGING
var top_left = new Point(0, 0);
var bottom_right = new Point(width, height);

//Rect around workspace for DEBUGGING
var top_left_buffer = new Point(x_buffer, y_buffer);
var bottom_right_buffer = new Point(width-x_buffer, height-y_buffer);

var q_width = (width-(2*x_buffer))/3;

//Quadrant 1
var q1_tl = new Point(x_buffer, y_buffer);
var q1_br = new Point(x_buffer+q_width*.25, height-y_buffer);
var q1_c = new Point(x_buffer+q_width/8, (y_buffer+(y_buffer/2)));

//Quadrant 2
var q2_tl = new Point(q_width/8+x_buffer, y_buffer);
var q2_br = new Point(q_width*1+ x_buffer, height-y_buffer);
var q2_c = new Point(x_buffer+q_width/2, (y_buffer+(y_buffer/2)));

//Quadrant 3
var q3_tl = new Point(q_width*1+x_buffer, y_buffer);
var q3_br = new Point(q_width*2+x_buffer, height-y_buffer);
var q3_c = new Point(x_buffer+q_width*1.5, (y_buffer+(y_buffer/2)));

//Quadrant 4
var q4_tl = new Point(q_width*2 + x_buffer, y_buffer);
var q4_br = new Point(q_width*3 + x_buffer, height-y_buffer);
var q4_c = new Point((q_width*2)+q_width/2 + x_buffer, (y_buffer+(y_buffer/2)));

// Event Data
var edu = {
        "home_school" : 
        {
            "title":"Home School",
            "subtitle": "Pre-K and Kindergarden",
            "location": "Tallahassee, FL",
            "date_start": new Date('April 1, 1992'),
            "date_finish": new Date('Jan 1, 1998'),
            "image": "../images/canvas_pics/home_school.jpg"
        },
        "florida_high" : 
        {
            "title":"Florida State University Elementary School",
            "subtitle": "1st-4th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 1, 1998'),
            "date_finish": new Date('Aug 1, 2002'),
            "image": "../images/canvas_pics/florida_high.jpg"
        },
        "sealey" : 
        {
            "title":"Sealey Elementary School",
            "subtitle": "5th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 1, 2002'),
            "date_finish": new Date('Aug 1, 2003'),
            "image": "../images/canvas_pics/sealey.jpg"
        },
        "raa_middle" : 
        {
            "title":"RAA Middle School",
            "subtitle": "6th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 1, 2003'),
            "date_finish": new Date('Aug 1, 2004'),
            "image": "../images/canvas_pics/raa.png"
        },
        "bellview" : 
        {
            "title":"Quantum Math and Science Magnet Program",
            "subtitle": "7th-8th grade",
            "location": "Tallahassee, FL",
            "date_start": new Date('April 1, 2004'),
            "date_finish": new Date('Jan 1, 2006'),
            "image": null
        },
        "high_school" :
        {
            "title":"Rickards IB Program",
            "subtitle": "International Baccalaureate Program",
            "location": "Tallahassee, FL",
            "date_start": new Date('Aug 18, 2006'),
            "date_finish": new Date('Aug 18, 2008'),
            "image": "../images/canvas_pics/rickards.jpg"
        },
        "nfcc" :
        {
            "title":"North FL Community College",
            "subtitle": null,
            "location": "Madison, FL",
            "date_start": new Date('Aug 18, 2008'),
            "date_finish": new Date('Aug 18, 2010'),
            "image": "../images/canvas_pics/nfcc.png"
        },
        "ncf" :
        {
            "title":"New College of FL",
            "subtitle": "Area of Concentration: Chemistry, Comp Sci",
            "location": "Sarasota, FL",
            "date_start": new Date('Aug 18, 2010'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/ncf.png"
        },
        "mlbco" :
        {
            "title":"MLB Co UAV Internship",
            "subtitle": "NASA airspace simulation internship",
            "location": "Mountain View, CA",
            "date_start": new Date('Jan 18, 2011'),
            "date_finish": new Date('June 18, 2012'),
            "image": "../images/canvas_pics/mlbco.jpg"
        },
        "nwnh" :
        {
            "title":"Northwest Natural Horsemanship",
            "subtitle": "Horse training",
            "location": "Fall City, WA",
            "date_start": new Date('June 30, 2011'),
            "date_finish": new Date('July 30, 2012'),
            "image": "../images/canvas_pics/nwnh.png"
        },
        "coursera_cryptography" :
        {
            "title":"Cryptography Coursera Course",
            "subtitle": "Cryptography I with Dan Boneh",
            "location": "Online",
            "date_start": new Date('Aug 18, 2012'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/coursera.png"
        },
        "coursera_nlp" :
        {
            "title":"Natural Language Processing Coursera Course",
            "subtitle": "NLP Course",
            "location": "Online",
            "date_start": new Date('Aug 18, 2012'),
            "date_finish": new Date('Aug 18, 2012'),
            "image": "../images/canvas_pics/coursera.png"
        },
         "calpoly" :
        {
            "title":"California Polytechnic University",
            "subtitle": "Major: General Engineering, Minor: CS",
            "location": "San Luis Obispo, CA",
            "date_start": new Date('Aug 18, 2012'),
            "date_finish": new Date('April 1, 2014'),
            "image": "../images/canvas_pics/calpoly.jpg"
        }, 
        "raytheon" :
        {
            "title":"Raytheon Cybersecurity Internship",
            "subtitle": "Cybersecurity Internship",
            "location": "Los Angeles, CA",
            "date_start": new Date('April 18, 2012'),
            "date_finish": new Date('July 30, 2014'),
            "image": "../images/canvas_pics/raytheon.png"
        }, 
        "ucf" :
        {
            "title":"University of Central FL",
            "subtitle": "Major: Mech Eng, Minors: CS, IT",
            "location": "Orlando, FL",
            "date_start": new Date('April 18, 2014'),
            "date_finish": new Date('April 30, 2016'),
            "image": "../images/canvas_pics/ucf.jpg"
        },
        "ruby" : 
        {
            "title":"Taught a Ruby on Rails Workshop",
            "subtitle": "Taught a RoR workshop for Engineers without Borders",
            "location": "Orlando, FL",
            "date_start": new Date('July 1, 2014'),
            "date_finish":new Date('Aug 1, 2014'),
            "image": "../images/canvas_pics/ruby_on_rails.jpg"
        },
        "ewb_pres" : 
        {
            "title":"President of Engineers without Borders - UCF",
            "subtitle": "Elected president of EWB",
            "location": "Orlando, FL",
            "date_start": new Date('Jan 1, 2014'),
            "date_finish": new Date('Jun 11, 2015'),
            "image": "../images/canvas_pics/ewb.jpg"
        },
        "co_bolder" : 
        {
            "title":"Design Global Engineer Local",
            "subtitle": "Course through University Co Boulder in Panama!",
            "location": "Panama City, Panama",
            "date_start": new Date('Jan 1, 2014'),
            "date_finish": new Date('Jun 11, 2015'),
            "image": "../images/canvas_pics/panama.jpg"
        },
        "solar_skill_share" : 
        {
            "title":"Solar Skill Share",
            "subtitle": "Taught a Solar Skill Share for Engineers without Borders",
            "location": "Orlando, FL",
            "date_start": new Date('Oct 1, 2015'),
            "date_finish":new Date('Oct 1, 2015'),
            "image": "../images/canvas_pics/solar_ss.jpg"
        },
        "coursera_mobile_apps" :
        {
            "title":"Mobile Apps Coursera Course",
            "subtitle": "Writting Android Apps",
            "location": "Sarasota, FL",
            "date_start": new Date('Jun 18, 2015'),
            "date_finish": new Date('July 18, 2015'),
            "image": "../images/canvas_pics/coursera.png"
        },
        "udacity_self_driving_cars" :
        {
            "title":"Self Driving Cars Udacity Course",
            "subtitle": "Artificial Intelligence for Robotics Udacity Course",
            "location": "Online",
            "date_start": new Date('Jun 18, 2015'),
            "date_finish": new Date('July 18, 2015'),
            "image": "../images/canvas_pics/udacity.png"
        }
};

var proj = {
        "4h" : 
        {
            "title":"JK Arabian Stables 4H Leader",
            "subtitle": "Lead a 4-H program at JK Arabian Stables",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2000'),
            "date_finish": new Date('Jan 1, 2002'),
            "image": "../images/canvas_pics/4h.jpg"
        },
        "parelli" : 
        {
            "title":"Parelli Natural Horsemanship",
            "subtitle": "Practiced and taught natural horse-man-ship",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2002'),
            "date_finish": new Date('Jan 1, 2008'),
            "image": "../images/canvas_pics/tedbeach.jpg"
        },
        "sci_girls" : 
        {
            "title":"Sci Girls Summer Camp",
            "subtitle": "Summer Camp with the FSU Magnet Lab",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2003'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/maglab.jpg"
        },
        "prop_scars" : 
        {
            "title": "Propellar Scars off Turkey Point",
            "subtitle": "Studied a timelapse of propscars off Turkey Point, FL",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2004'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": null
        },
        "nasas_great_moonbuggy_race" : 
        {
            "title":"Participated in NASA's Great Moonbuggy Race!",
            "subtitle": "Co-piloted the moonbuggy :)",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2005'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/moonbuggy.jpg"
        },
        "green_industries" : 
        {
            "title":"Green Industries Internship",
            "subtitle": "Researched EMERGY analysis for the site",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2006'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/gi.jpg"
        },
        "new_moon_nights" : 
        {
            "title":"Hosted 'New Moon Nights' at local observatory",
            "subtitle": "Educational workshop once/month on the new moon",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2007'),
            "date_finish": new Date('Jan 1, 2004'),
            "image": "../images/canvas_pics/nmn.png"
        },
        "italian ice" : 
        {
            "title":"Peace, Love & Italian Ice, LLC",
            "subtitle": "Started an Italian Ice Vending Business",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2008'),
            "date_finish": new Date('Jan 1, 2010'),
            "image": "../images/canvas_pics/pli.png"
        },
        "real estate business" : 
        {
            "title":"Destiny Investment Group, LLC",
            "subtitle": "Became a partner in a realestate business flipping houses",
            "location": "Tallahassee, FL",
            "date_start": new Date('Jan 1, 2009'),
            "date_finish": new Date('Jan 1, 2010'),
            "image": null
        },
        "wind turbine" : 
        {
            "title":"Beach Front Wind Turbine",
            "subtitle": "Built a wind turbine with treadmill motor",
            "location": "Sarasota, FL",
            "date_start": new Date('Jan 1, 2011'),
            "date_finish": new Date('March 1, 2011'),
            "image": null
        },
        "robotic chess" : 
        {
            "title":"Robotic Chess Game",
            "subtitle": "Helped code for a robotic chess game at FAU",
            "location": "Boca Raton, FL",
            "date_start": new Date('Jan 1, 2010'),
            "date_finish": new Date('March 1, 2010'),
            "image": "../images/canvas_pics/robotic_chess.png"
        },
        "rc" : 
        {
            "title":"DIY Foam RC Craft",
            "subtitle": "Built a foam UAV and tried to fly it...crashed",
            "location": "Sarasota, FL",
            "date_start": new Date('Jan 1, 2011'),
            "date_finish": new Date('March 1, 2011'),
            "image": "../images/canvas_pics/rc.png"
        },
        "grid_alternatives" : 
        {
            "title":"Grid Alternatives Installation",
            "subtitle": "Assisted installing solar systems for houses!",
            "location": "San Luis Obispo, FL",
            "date_start": new Date('Jan 1, 2011'),
            "date_finish": new Date('March 1, 2011'),
            "image": "../images/canvas_pics/grid_alternatives.jpg"
        },
        "bee_keeping" : 
        {
            "title":"Bee Keeping",
            "subtitle": "Started keeping a few hundred thousand bees and stealing their honey when they got too sweet ;)",
            "location": "Orlando, FL",
            "date_start": new Date('Jan 1, 2015'),
            "date_finish": new Date(),
            "image": "../images/canvas_pics/bees.jpg"
        },
        "at2" : 
        {
            "title":"Appropriate Tech Toolkit",
            "subtitle": "Built a bla",
            "location": "Orlando, FL",
            "date_start": new Date('May 1, 2014'),
            "date_finish": new Date(),
            "image": null
        },
        "epa" : 
        {
            "title":"Engineering Personal Assistant",
            "subtitle": "Built a bla",
            "location": "Orlando, FL",
            "date_start": new Date('Jan 1, 2015'),
            "date_finish": new Date(),
            "image": null
        }
        
};

var travel = {
        "north carolina" : 
        {
            "title":"Earthskills Rendezvous",
            "subtitle": null,
            "location": "Layfayette, GA",
            "date_start": new Date('June 1, 2005'),
            "date_finish": new Date('July 1, 2005'),
            "image": null
        },
        "quest out west" : 
        {
            "title":"Vacationed all over western USA",
            "subtitle": null,
            "location": "UT, CO, NM, AZ, NV",
            "date_start": new Date('April 1, 2007'),
            "date_finish": new Date('Aug 1, 2007'),
            "image": null
        },
        "st croix" : 
        {
            "title":"Bushskills Rendezvous",
            "subtitle": "Virgin Island Sustianable Institute",
            "location": "St Croix, Virgin Islands",
            "date_start": new Date('April 1, 2010'),
            "date_finish": new Date('June 1, 2010'),
            "image": null
        },
        "sailtrip" : 
        {
            "title":"Vacationed all over western USA",
            "subtitle": null,
            "location": "UT, CO, NM, AZ, NV",
            "date_start": new Date('April 1, 2007'),
            "date_finish": new Date('Aug 1, 2007'),
            "image": null
        },
        "usa road trip" : 
        {
            "title":"Road Trip! FL to CA",
            "subtitle": null,
            "location": "FL, AL, LA, AK, MS, KA, CO, UT, NV, CA",
            "date_start": new Date('April 1, 2012'),
            "date_finish": new Date('June 1, 2012'),
            "image": null
        },
        "green gulch farm" : 
        {
            "title":"Green Gulch Farm Zen Center",
            "subtitle": "Worktrade",
            "location": "San Francisco, CA",
            "date_start": new Date('March 1, 2011'),
            "date_finish": new Date('April 1, 2011'),
            "image": null
        },
        "rdi" : 
        {
            "title":"Regenerative Design Institute Internship",
            "subtitle": "Natural Animal Care internship",
            "location": "Bolinas, CA",
            "date_start": new Date('April 1, 2011'),
            "date_finish": new Date('May 1, 2011'),
            "image": null
        },
        "washington" : 
        {
            "title":"Washington NW Natural Horsemanship Internship",
            "subtitle": "Parelli horse training internship",
            "location": "Fall City, Washington",
            "date_start": new Date('May 1, 2011'),
            "date_finish": new Date('June 1, 2011'),
            "image": null
        },
        "ny" : 
        {
            "title":"Silver Bay YMCA Life Guard",
            "subtitle": "Life guarded on Lake George",
            "location": "Fall City, Washington",
            "date_start": new Date('June 1, 2011'),
            "date_finish": new Date('July 1, 2011'),
            "image": null
        },
        
        "montreal" : 
        {
            "title":"Visiting Montreal",
            "subtitle": null,
            "location": "Ste-Marthe, Canada",
            "date_start": new Date('July 1, 2011'),
            "date_finish": new Date('Aug 1, 2011'),
            "image": null
        },
        "ste" : 
        {
            "title":"Horsemanship Training",
            "subtitle": "Work-trade",
            "location": "Ste-Marthe, Canada",
            "date_start": new Date('Aug 1, 2011'),
            "date_finish": new Date('September 1, 2011'),
            "image": null
        },
        "usa_road_trip_again" : 
        {
            "title":"Road Trip! FL to CA",
            "subtitle": null,
            "location": "FL, AL, LA, AK, MS, KA, CO, UT, NV, CA",
            "date_start": new Date('April 1, 2012'),
            "date_finish": new Date('June 1, 2012'),
            "image": null
        },
        "north_carolina" : 
        {
            "title":"Visiting Friends",
            "subtitle": null,
            "location": "Ste-Marthe, Canada",
            "date_start": new Date('September 1, 2010'),
            "date_finish": new Date('Oct 1, 2010'),
            "image": null
        },
        "bahamas" : 
        {
            "title":"Vacation",
            "subtitle": "My first shark dive!",
            "location": "Bahamas",
            "date_start": new Date('April 1, 2014'),
            "date_finish": new Date('June 1, 2014'),
            "image": null
        },
        "key west" : 
        {
            "title":"Dive Trip!",
            "subtitle": "My first night dive!!",
            "location": "Key West, FL",
            "date_start": new Date('May 5, 2015'),
            "date_finish": new Date('May 20, 2015'),
            "image": null
        },
        "minoqua" : 
        {
            "title":"Family Vacation",
            "subtitle": null,
            "location": "Crivitz, WI",
            "date_start": new Date('Aug 5, 2015'),
            "date_finish": new Date('Aug 20, 2015'),
            "image": null
        }
};

function run_debug() {
    //Rects around quadrants for DEBUGGING
    var shape = new Shape.Rectangle(top_left, bottom_right);
    shape.strokeColor = 'white';
    
    var shape = new Shape.Rectangle(top_left_buffer, bottom_right_buffer);
    shape.strokeColor = 'red';
    
    var shape = new Shape.Rectangle(q1_tl, q1_br);
    shape.strokeColor = 'purple';
    var shape = new Shape.Circle(q1_c, 5);
    shape.strokeColor = 'purple';
    var shape = new Shape.Rectangle(q2_tl, q2_br);
    shape.strokeColor = 'yellow';
    var shape = new Shape.Circle(q2_c, 5);
    shape.strokeColor = 'yellow';
    var shape = new Shape.Rectangle(q3_tl, q3_br);
    shape.strokeColor = 'pink';
    var shape = new Shape.Circle(q3_c, 5);
    shape.strokeColor = 'pink';
    var shape = new Shape.Rectangle(q4_tl, q4_br);
    shape.strokeColor = 'blue';
    var shape = new Shape.Circle(q4_c, 5);
    shape.strokeColor = 'blue';
}

function add_layout() {
    
    //Create an invisible line for years with proper spacing
    var space = line_length/age;
    for (var i = date_current.getFullYear(); i>birth_date.getFullYear(); i--) {
        if (i%2 != 0) {
            var date_text = new PointText({
                point: q1_c + [-30, -(i-2015)*space],
                content: "____",
                fillColor: white,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 25
            });
        } else {
            var date_text = new PointText({
                point: q1_c + [-30, -(i-2015)*space],
                content: i,
                fillColor: white,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 25
            });
        }
    }

    var end_date_text = new PointText({
        point: q1_c+ [-30, line_length-30],
        // content: birth_date.getFullYear(),
        content: "Born",
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 25
    });    
}

function add_education_layout() {
    //Create a line and title for Education
    var education_line = new Path( {
        strokeColor: white,
        strokeWidth: 5,
        strokeCap: 'round'
    })
    
    education_line.moveTo(q2_c);
    education_line.lineTo(q2_c+[0,line_length]);
    
    var edu_title = new PointText({
        point: q2_c - [55,y_buffer/2],
        content: "Education/Work",
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 25
    });
}

function add_projects_layout() {
    //Create a line and title for Projects
    var project_line = new Path( {
        strokeColor: white,
        strokeWidth: 5,
        strokeCap: 'round'
    })
    
    project_line.moveTo(q3_c);
    project_line.lineTo(q3_c+[0,line_length]);
    
    var proj_title = new PointText({
        point: q3_c - [55,y_buffer/2],
        content: "Personal Projects",
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 25
    });
}

function add_travel_layout() {
    //Create a line for Travel
    var travel_line = new Path( {
        strokeColor: white,
        strokeWidth: 5,
        strokeCap: 'round'
    })
    
    travel_line.moveTo(q4_c);
    travel_line.lineTo(q4_c+[0,line_length]);
    
    var travel_title = new PointText({
        point: q4_c - [45,y_buffer/2],
        content: "Travel",
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 25
    });
}

var event_spacing = 15;

function get_max_events() {
    // Returns the maximum number of events for edu, travel and proj objects
    var max = 0;
    var i = 0;
    for (var key in proj) { i++; }
    if (i > max) { max = i; }
    i=0;
    for (var key in edu) { i++; }
    if (i > max) { max = i; }
    i=0;
    for (var key in travel) { i++; }
    if (i > max) { max = i; }
    return max;
}

// // Test: Add an event for every month and make sure alignment works
// for (var i=0;i<12*(age);i++) {
//     var y_position = line_length - i*event_spacing;
//     var new_pt = q2_c + [0, y_position];
//     //Add circle
//     var edu_event = new Shape.Circle(new_pt, 8);
//     edu_event.strokeColor = transparent_white;
//     edu_event.strokeWidth = 5;
// }

//Test
// get_segments("hello I am", 15);
// get_segments("hello I am a string with", 15);
// get_segments("hello I am a string with segments that are great", 15);

function get_segments(str, max_chars) {
    //Split strings into spaces
    var split_str = str.split(" ");
    var segments = [""];
    
    // While the end of the array isn't reached
    var i = 0;
    var j = 0;
    while (split_str[i]) {
        // If there is room to add another word, add it
        if ( (segments[j]+ " " + split_str[i]).length < max_chars) {
            //Add another word to segments j
            segments[j] = segments[j] + " " + split_str[i];
            i++;
        } else {
            // Increment segments and i
            segments.push(split_str[i]);
            j++;
            i++;
        }
    }
    // console.log("Final Segments: "+segments);
    return segments
}

function add_simple_event(quadrant, obj) {
    var months =    (Math.abs(obj.date_start.getFullYear() - birth_date.getFullYear()))*12;
    console.log("years in months: "+months);
    
    months += Math.abs(obj.date_start.getMonth() - obj.date_finish.getMonth());
    console.log("months: "+months);
    
    var y_position = line_length - months*event_spacing;
    console.log("y_pos: "+y_position);
    
    if (quadrant == 1) {
        var new_pt = q1_c + [0, y_position];
    } else if (quadrant == 2) {
        var new_pt = q2_c + [0, y_position];
    } else if (quadrant == 3) {
        var new_pt = q3_c + [0, y_position];
    } else if (quadrant == 4) {
        var new_pt = q4_c + [0, y_position];
    } else {
        console.error("Choose quadrant in range 1-4")
    }
    
    //Add circle
    var edu_event = new Shape.Circle(new_pt, 8);
    edu_event.strokeColor = white;
    edu_event.strokeWidth = 5;
    
    //Add text
    var title = new PointText({
        point: new_pt+[20,0],
        content: obj.title,
        fillColor: white,
        fontFamily: font,
        fontWeight: weight,
        fontSize: 12
    });
    
    //Create the background rect for the image MAYBE GET RID OF THIS!!
    var big_rect_w = 150;
    var big_rect_h = -120;
    var size = new Size(big_rect_w, big_rect_h);
    var background_rect = new Path.Rectangle((new_pt+[-175,50]), size);
    background_rect.fillColor = white;
    background_rect.visible = false;
    
    //Create the object image and assign it a source
    var raster = new Raster();
    if (obj.image == null) { raster.source = '../images/canvas_pics/no-image.png'; } 
    else { raster.source = obj.image; }
    raster.visible = false;
    raster.bringToFront();
    raster.position = background_rect.bounds.center;
    
    //Add the subtitle information below the image
    var group = new Group();
    var i = 0;
    var text_rect = new Rectangle();
    if (obj.subtitle != null) {
        //Chars_per_line should be changed to be a function of box width
        var chars_per_line = 25;
        var segments = get_segments(obj.subtitle, chars_per_line);
        for (i=0; i<segments.length;i++) {
            // Make a line of text for each string segment
            var text = new PointText({
                content: segments[i],
                position: (background_rect.bounds.bottomCenter + [0, title.bounds.height*(i+1)]),
                fillColor: invisible,
                // visible: false,
                fontFamily: font,
                fontWeight: weight,
                fontSize: 11,
            });
            //Add this as a child to subtitle
            group.addChild(text);
        }
        
        // Create the background rect for the text
        var size = new Size(big_rect_w,title.bounds.height*(i+1));
        var text_rect = new Path.Rectangle(background_rect.bounds.bottomLeft, size, new Size(5,5));
        text_rect.fillColor = white;
        text_rect.visible = false;
        text_rect.sendToBack();
    }
    
    title.on('mousemove', function() {
        //Bold the text that is selected
        this.fontWeight = 'bold',
        edu_event.strokeColor = transparent_blue,
        raster.visible = true,
        group.fillColor = blue,
        // background_rect.visible = true,
        text_rect.visible = true
    });
    
    title.on('mouseleave', function() { 
        this.fontWeight = weight,
        edu_event.strokeColor = white,
        raster.visible = false,
        group.fillColor = invisible,
        // background_rect.visible = false,
        text_rect.visible = false
    });
}

function add_education_events() {
    for (var key in edu) {
        if (edu.hasOwnProperty(key)) {
            var obj = edu[key];
            add_simple_event(2, obj);
        }
    }
}

function add_project_events() {
    for (var key in proj) {
        if (proj.hasOwnProperty(key)) {
            var obj = proj[key];
            add_simple_event(3, obj);
        }
    }
}

function add_travel_events() {
    for (var key in travel) {
        if (travel.hasOwnProperty(key)) {
            var obj = travel[key];
            add_simple_event(4, obj);
        }
    }
}

function onMouseMove(event) {
	project.activeLayer.selected = false;
	if (event.item) {
	    // For debugging only
		//event.item.selected = true;
	}
}

add_layout();
// run_debug();
add_education_layout();
add_projects_layout();
add_travel_layout();
add_education_events();
add_project_events();
add_travel_events();